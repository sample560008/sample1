package Programs;

import java.util.Scanner;

public class Factorial {
	
	public void display() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number :");
		int n = sc.nextInt();
		int result = 1;
		for(int i=1;i<=n;i++) {
			result*=i;
		}
		
		System.out.println(result);
	}
	
	public static void main(String[] args) {
		Factorial obj = new Factorial();
		obj.display();
	}

}
